﻿using System;
using System.Collections.Generic;
using System.Text;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;
using System.Windows;
using System.Collections;
using System.Data;

namespace Demetra21
{
    public class Camera
    {
        public string filename = "C://xampp//htdocs//plastenik//images//cameras//1.jpg";

        public PictureBox pictureBox;
        public List<string> webcams = new List<string>();
        public Bitmap bitmap;
        public Image image;
        public bool convertImage = false;


        private FilterInfoCollection videoDevices;
        private VideoCaptureDevice videoSource;
        private Timer timer = new Timer();

        private int _cameraIndex = 0;
        public int cameraIndex
        {
            set
            {
                _cameraIndex = value;
                initializeWebCam();
                initializeTimer();
            }
            get { return _cameraIndex; }
        }

        public Camera(PictureBox _pictureBox)
        {
            pictureBox = _pictureBox;
        }

        public void getDevices()
        {
            webcams.Clear();

            FilterInfoCollection _videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            for (int i = 0; i < _videoDevices.Count; i++)
                webcams.Add(_videoDevices[i].Name);
        }

        private void initializeTimer()
        {
            timer.Interval = 500;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        public void changeInterval(int interval)
        {
            timer.Interval = interval;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            try
            {
                if (File.Exists(filename))
                    File.Delete(filename);

                if (convertImage)
                {
                    image = resizeImage(bitmap, 350);
                    image.Save(filename, ImageFormat.Jpeg);
                }
                else
                {
                    bitmap.Save(filename, ImageFormat.Jpeg);
                }
            }
            catch { }
            timer.Start();
        }

        public void initializeWebCam()
        {
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            videoSource = new VideoCaptureDevice(videoDevices[cameraIndex].MonikerString);
            videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);
        }

        public void start()
        {
            initializeWebCam();
            videoSource.Start();
        }

        public void stop()
        {
            videoSource.Stop();
        }


        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                bitmap = (Bitmap)eventArgs.Frame.Clone();
                pictureBox.Image = bitmap;
            }
            catch { }
        }

        public Image resizeImage(Image FullsizeImage, int NewHeight)
        {
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            int NewWidth = FullsizeImage.Width * NewHeight / FullsizeImage.Height;
            System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
            FullsizeImage.Dispose();
            return NewImage;
        }
    }
}
