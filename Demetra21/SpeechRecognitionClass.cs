﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Speech.Recognition;
using System.Windows.Forms;


namespace Demetra21
{
    public class SpeechRecognition
    {
        private SpeechRecognitionEngine speechRecognizer = new SpeechRecognitionEngine();
        private Demetra21 demetra21;
        private bool testMode = false;


        public SpeechRecognition(Demetra21 _plastenik)
        {
            demetra21 = _plastenik;

            speechRecognizer.SetInputToDefaultAudioDevice();

            if (testMode)
                speechRecognizer.LoadGrammar(new DictationGrammar());

            else
            {
                Choices commands = new Choices();
                commands.Add(new string[] { "and lots", "rules", "would", "seattle" });
                GrammarBuilder gb = new GrammarBuilder();
                gb.Append(commands);
                Grammar g = new Grammar(gb);
                speechRecognizer.LoadGrammar(g);
            }

            speechRecognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(speechRecognizer_SpeechRecognized);
            speechRecognizer.RecognizeAsync(RecognizeMode.Multiple);

        }

        private void speechRecognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (!testMode)
            {
                if (Config.speechRecognition)
                {
                    switch (e.Result.Text)
                    {
                        case "and lots":
                            if (demetra21.getCooler() > 0)
                                demetra21.setCooler("0", true);
                            else demetra21.setCooler("100", true);
                            break;

                        case "rules":
                            if (demetra21.getRoof() > 0)
                                demetra21.setRoof("0", true);
                            else demetra21.setRoof("100", true);

                            break;

                        case "seattle":
                            if (demetra21.getLight() > 0)
                                demetra21.setLight("0", true);
                            else demetra21.setLight("100",true);

                            break;

                            

                        case "would":
                            demetra21.setWater("true");
                            System.Threading.Thread.Sleep(1500);
                            demetra21.setWater("false");
                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show(e.Result.Text);
            }
        }
    }
}
