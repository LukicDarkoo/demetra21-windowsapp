﻿using System;
using System.IO.Ports;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace Demetra21
{
    public class Demetra21
    {
        private SerialPort serialPort = new SerialPort();
        private WebClient webClientGetter = new WebClient();
        private WebClient webClientSetter = new WebClient();
        private FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();

        public string portName = "COM3";


        //auto
        private bool _auto = false;
        public void setAuto(string value, bool update = false)
        {
            bool _value = Convert.ToBoolean(value);
            if (_auto != _value)
            {
                _auto = _value;

                if (_value)
                {
                    serialPort.WriteLine("a1");
                    speech("Automatska regulacija pokrenuta");
                }
                else
                {
                    serialPort.WriteLine("a0");
                    speech("Automatska regulacija iskljucena");
                }
            }

            if (update)
                setValue("auto", value);
        }
        public bool getAuto()
        {
            return _auto;
        }


        private int _autoIllumination = 0;
        public void setAutoIllumination(string value, bool update = false)
        {
            int _value = Convert.ToInt32(value);

            if (_autoIllumination != _value)
            {
                _autoIllumination = _value;
                serialPort.WriteLine("as" + _value);
                speech("Automatska regulacija svjetlosti je postavljena na " + value + " luksa");
            }

            if (update)
                setValue("auto_illumination", value);
        }
        public int getAutoIllumination()
        {
            return _autoIllumination;
        }


        //aktuatori
        private bool _heater = false;
        public void setHeater(string value, bool update = false)
        {
            bool _value = Convert.ToBoolean(value);
            if (_heater != _value)
            {
                _heater = _value;

                if (_value)
                {
                    serialPort.WriteLine("g255");
                    speech("Grijač je uključen");
                }
                else
                {
                    serialPort.WriteLine("g0");
                    speech("Grijač je isključen");
                }
            }

            if (update)
                setValue("heater", value);
        }
        public bool getHeater()
        {
            return _heater;
        }

        private int _doors = 0;
        public void setDoors(string value, bool update = false)
        {
            int _value = Convert.ToInt32(value);

            if (_doors != _value)
            {
                _doors = _value;
                serialPort.WriteLine("v" + Helper.Map(_value, 0, 100, 70, 43).ToString());
                speech("Vrata su podgnuta na " + value + " procenata");
            }

            if (update)
                setValue("doors", value);
        }
        public int getDoors()
        {
            return _doors;
        }

        private int _roof = 0;
        public void setRoof(string value, bool update = false)
        {
            int _value = Convert.ToInt32(value);

            if (_roof != _value)
            {
                _roof = _value;
                serialPort.WriteLine("r" + Helper.Map(_value, 0, 100, 120, 5).ToString());
                speech("Krov je podgnut na " + value + " procenata");
            }

            if (update)
                setValue("roof", value);
        }
        public int getRoof()
        {
            return _roof;
        }

        private int _light = 0;
        public void setLight(string value, bool update = false)
        {
            int _value = Convert.ToInt32(value);
            if (_light != _value)
            {
                _light = _value;
                speech("Osvjetljenje " + value + " procenata");
                serialPort.WriteLine("s" + Helper.Map(_value, 0, 100, 0, 255).ToString());
            }

            if (update)
                setValue("light", value);
        }
        public int getLight()
        {
            return _light;
        }

        private bool _water = false;
        public void setWater(string value, bool update = false)
        {
            bool _value = Convert.ToBoolean(value);

            if (_water != _value)
            {
                _water = _value;

                if (_value)
                {
                    serialPort.WriteLine("p255");
                    speech("Pumpa je uključena");
                }
                else
                {
                    serialPort.WriteLine("p0");
                    speech("Pumpa je isključena");
                }
            }

            if (update)
                setValue("water", value);
        }
        public bool getWater()
        {
            return _water;
        }

        private int _cooler = 0;
        public void setCooler(string value, bool update = false)
        {
            int _value = Convert.ToInt32(value);

            if (_cooler != _value)
            {
                _cooler = _value;
                serialPort.WriteLine("k" + Helper.Map(_value, 0, 100, 0, 255).ToString());
                speech("Jačina ventilacije je postavljena na " + value + " procenata");
            }

            if (update)
                setValue("cooler", value);
        }
        public int getCooler()
        {
            return _cooler;
        }


        //senzori
        private int _temperature = 0;
        public void setTemperature(string value, bool update = true)
        {
            int _value = Convert.ToInt32(value);

            if (_value != _temperature)
            {
                _temperature = _value;

                if (update)
                    setValue("temperature", value);
            }
        }
        public int getTemperature()
        {
            return _temperature;
        }

        private int _illumination = 0;
        public void setIllumination(string value, bool update = true)
        {
            int _value = Convert.ToInt32(value);

            if (Math.Abs(_illumination - _value) > 60)
            {
                _illumination = _value;

                if (update)
                    setValue("illumination", value);
            }
        }
        public int getIllumination()
        {
            return _illumination;
        }

        private int _airHumidity = 0;
        public void setAirHumidity(string value, bool update = true)
        {
            int _value = Convert.ToInt32(value);

            if (_value != _airHumidity)
            {
                _airHumidity = _value;

                if (update)
                    setValue("air_humidity", value);
            }
        }
        public int getAirHumidity()
        {
            return _airHumidity;
        }

        private int _groundHumidity = 0;
        public void setGroundHumidity(string value, bool update = true)
        {
            int _value = Convert.ToInt32(value);

            if (_value != _groundHumidity)
            {
                _groundHumidity = _value;

                if (update)
                    setValue("ground_humidity", value);
            }
        }
        public int getGroundHumidity()
        {
            return _groundHumidity;
        }


        public Demetra21()
        {
            serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
            webClientGetter.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClientGetter_DownloadStringCompleted);
            fileSystemWatcher.Changed += new FileSystemEventHandler(fileSystemWatcher_Changed);
        }


        private string makeSetUrl()
        {
            return Config.serverAddress + "/communicator/userSet/" + Config.id + "/";
        }

        private string makeGetUrl(bool first = false)
        {
            return Config.serverAddress + "/communicator/userGet/" + Config.id + "/" + (first ? "?first" : "");
        }

        private string makePath()
        {
            return Config.jsonLocation + "//data//" + Config.id + ".txt";
        }

        private void jsonToPlastenik(string json)
        {
            try
            {
                JObject data = JObject.Parse(json);

                setHeater(data["heater"].ToString());
                setCooler(data["cooler"].ToString());
                setDoors(data["doors"].ToString());
                setLight(data["light"].ToString());
                setWater(data["water"].ToString());
                setRoof(data["roof"].ToString());

                setAuto(data["auto"].ToString());
                setAutoIllumination(data["auto_illumination"].ToString());
                
            }
            catch { }
        }


        void webClientGetter_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (!Config.fastControl)
            {
                jsonToPlastenik(e.Result.ToString());
                webClientGetter.DownloadStringAsync(new Uri(makeGetUrl()));
            }
        }

        void fileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (Config.fastControl)
            {
                try
                {
                    StreamReader reader = new StreamReader(makePath());
                    string data = reader.ReadLine();
                    reader.Close();
                    jsonToPlastenik(data);
                }
                catch { }
            }
        }

        public void start()
        {
            serialPort.Close();
            serialPort.PortName = portName;
            serialPort.BaudRate = 115200;
            serialPort.Open();

            webClientGetter.DownloadStringAsync(new Uri(makeGetUrl(true)));
            fileSystemWatcher.Path = Path.GetDirectoryName(makePath());
            fileSystemWatcher.EnableRaisingEvents = true;            
        }

        private void setValue(string key, string value)
        {
            try
            {
                webClientSetter.DownloadStringAsync(new Uri(makeSetUrl() + "?" + key + "=" + value));
            }
            catch { }
        }

        private void speech(string text)
        {
            if (Config.enableSpeak)
                Helper.Speech(text);
        }

        private void serialPort_DataReceived(object source, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string data = serialPort.ReadLine();

            if (data.Length > 0)
            {
                switch (data.Substring(0, 1))
                {
                    case "t":
                        setTemperature(data.Substring(1));
                    break;

                    case "v":
                        setAirHumidity(data.Substring(1));
                    break;

                    case "z":
                        setGroundHumidity(data.Substring(1));
                        break;

                    case "s":
                        setIllumination(data.Substring(1));
                        break;
                }
            }
        }
    }
}
