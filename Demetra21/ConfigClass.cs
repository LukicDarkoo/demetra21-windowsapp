﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demetra21
{
    static class Config
    {
        public static string id;
        public static string password;

        public static string serverAddress;
        public static string jsonLocation;
        public static bool fastControl = true;

        public static bool speechRecognition = false;
        public static bool enableSpeak = false;
    }
}
