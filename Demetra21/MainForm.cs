﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace Demetra21
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeClasses();
        }

        public string greenhouseId = "1";

        public Demetra21 demetra21;
        public Camera camera;
        public SpeechRecognition speechRecognition;

        private List<string> ports = new List<string>();


        private void InitializeClasses()
        {
            demetra21 = new Demetra21();
            camera = new Camera(pictureBox1);
            speechRecognition = new SpeechRecognition(demetra21);
        }

        private void getPorts()
        {
            ports.Clear();
            comboBox2.Items.Clear();

            foreach (Helper.COMPortInfo comPort in Helper.COMPortInfo.GetCOMPortsInfo())
            {
                ports.Add(comPort.Name);
                comboBox2.Items.Add(comPort.Name + " " + comPort.Description);
            }
        }

        private void getCameras()
        {
            comboBox1.Items.Clear();
            camera.getDevices();
            foreach (string cam in camera.webcams)
                comboBox1.Items.Add(cam);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            camera.cameraIndex = comboBox1.SelectedIndex;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Config.enableSpeak = checkBox2.Checked;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            getPorts();
            getCameras();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            getPorts();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Config.speechRecognition = checkBox1.Checked;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Config.serverAddress = textBox1.Text;
            Config.jsonLocation = textBox2.Text;
            Config.id = textBox3.Text;
            Config.password = textBox4.Text;
            Config.fastControl = checkBox3.Checked;

            demetra21.portName = ports[comboBox2.SelectedIndex];
            demetra21.start();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            camera.stop();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.Enabled = label1.Enabled = checkBox3.Checked;
            textBox1.Enabled = label3.Enabled = !checkBox3.Checked;
            Config.fastControl = checkBox3.Checked;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            getCameras();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            camera.cameraIndex = comboBox1.SelectedIndex;
            camera.start();
        }

    }
}
